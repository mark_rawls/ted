#!/usr/bin/env ruby

Configuration.for('app') {
	editor "subl"
	browser "google-chrome"
	projectDir Dir.home + "/Projects"
	extension ".rb"
	compiler "ruby"
	interpreter "ruby"
	executableExtension ".rb"
	executablePath "."
	sourcePath "."
	compilerOptions ""
}