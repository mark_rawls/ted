# README #


This repository hosts TED, the Tandem Enabled Development service. TED works in tandem with Rapid (the Rapid Acclimation of Projects Into Deployment toolkit) to help you streamline your development process in an extremely efficient manner. TED ships with its own built in package manager to install environment-aware scripts. The default scripts ship with a large degree of content aware intelligence, all others are user defined to their best of their degree.

What sets TED apart from other similar services, should they exist, is that TED supports any "script" in any language so long as their interpreter line or binary is properly formed. The only languages not supported are ones that require a special prepended command to run, such as java ("java Program.class"), but such support will be implemented in a later version.



### Overview ###

* Content/environment aware script injection service
* Version 0.12
* Open Source
* Self contained installer/package manner
* Works with Rapid

### Commands ###

TED holds whatever commands you give it. To view the available commands you have, run

```
#!bash

$ ted list
```

The list command will give you a readout of the files and folders in your root directory. You can append any directory onto there to list sub commands and sub directories. You can chain these infinitely.


```
#!bash

$ ted list
# git  rails  python
$ ted list git
# init  destroy  rollback  other
$ ted list git/other
# upload  merge  branch  fetch
```

To run any of those commands, it is as simple as typing in the name and the appropriate arguments. Argument handling is done intelligently and will transfer in verbatim everything you put after the script call.


```
#!bash

$ ted git/init git@github.com:your_name/project.git
# git output...
```

### TODO ###
* Add support for Java programs
* Add docs for creating commands