#!/usr/bin/env ruby

require 'trollop'
require 'highline/import'

$library_dir = Dir.home() + "/.config/ted/lib"
$resource_dir = Dir.home() + "/.config/ted/resources"
$starting_dir = Dir.pwd()

global_opts = Trollop::options do
	version "Ted 1.1.2 (c) 2014 Quantum Software"

	banner <<-EOS
Ted is a utility for having a customizable and documented library of anything from shortcuts to large programs. Easily extensible, with user-defined support.
EOS
	
	opt :recursive, "Run all of the files in a directory", :short => "-r", :default => false
	opt :install, "Install a package .zip file", :short => "-i", :type => String
	opt :package, "Create a packaged .zip file", :short => "-p", :type => String
end

def execute(files)
	files.each do |file|
		if File.file?(file) #yes, I named the block variable 'file' just for the grammar
			directive_dir = Dir.pwd()
			Dir.chdir($starting_dir)
			system(directive_dir + "/" + file, *ARGV)
		elsif File.directory?(file)
			Dir.chdir(file)
			execute(Dir.glob("*"))
			Dir.chdir("..")
		end
	end
end

if global_opts[:install] != nil
	originalDirectory = Dir.pwd()
	require 'fileutils'
	include FileUtils
	mkdir_p($resource_dir + "/tmp")
	system("unzip", global_opts[:install], "-d", $resource_dir + "/tmp")
	Dir.chdir($resource_dir + "/tmp") #cwd: $resource_dir/tmp
	newFiles = Dir.glob("*")

	newFiles.each do |folder|
		Dir.chdir(folder) #cwd: tmp/projectFolder
		puts HighLine.color(folder, :green)
		rubyFiles = Dir.glob("*.rb")

		rubyFiles.each do |executable|
			system("chmod", "+x", executable)
		end

		if File.exists?("./install.rb")
			require './install.rb'
			doSetupRoutine()
			rm("./install.rb")
		end
			
		if Dir.exists?("../../" + folder)
			installFiles = Dir.glob("*")

			Dir.chdir("../../" + folder) #cwd: $resource_dir/projectFolder

			installFiles.each do |fileName|
				if File.exists?(fileName)
					rm_rf(fileName)
					mv("../tmp/" + folder + "/" + fileName, ".")
				else
					next
				end
			end

			Dir.chdir($resource_dir + "/tmp") #cwd: $resource_dir/tmp
		else
			Dir.chdir("..") #cwd: $resource_dir/tmp
			cp_r(folder, "..")
		end
	end

	Dir.chdir("..")

	rm_rf("tmp")

	exit 0
end

if global_opts[:package] != nil
	system("zip", "-r", ARGV[-1], global_opts[:package])
	
	exit 0
end

if ARGV[0] == "list" && ARGV[1] == nil
	Dir.chdir($resource_dir)

	file_list = Dir.glob("*")
	file_list.each do |f|
		puts HighLine.color(f, :cyan)
	end
elsif ARGV[0] == "list" && ARGV[1] != nil
	Dir.chdir($resource_dir + "/" + ARGV[1])

	file_list = Dir.glob("*")
	file_list.each do |f|
		puts HighLine.color(f, :cyan)
	end
elsif ARGV[0] != nil
	Dir.chdir($resource_dir)
	directive = ARGV.shift
	if File.directory?(directive) && global_opts[:recursive] == false
		puts HighLine.color("Please enable recursion if you wish to run a whole directory.")
		exit 1
	elsif File.directory?(directive) && global_opts[:recursive] == true
		Dir.chdir(directive)
		execute(Dir.glob("*"))
	elsif File.file?(directive)
		execute([directive])
	end
end