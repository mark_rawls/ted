#!/usr/bin/env ruby

raise "Must run as root" unless Process.uid == 0

require 'fileutils'
include FileUtils

if File.exists?("/usr/bin/ted")
	rm("/usr/bin/ted")
	cp("./ted", "/usr/bin/ted")
else
	cp("./ted", "/usr/bin/ted")
end

mkdir_p(Dir.home + "/.config/ted/resources")
mkdir_p(Dir.home + "/.config/ted/lib")